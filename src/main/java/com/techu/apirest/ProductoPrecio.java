package com.techu.apirest;

public class ProductoPrecio {

    private Integer id;
    private Double precio;

    public ProductoPrecio(Integer id, Double precio) {
        this.id = id;
        this.precio = precio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
