package com.techu.apirest.service;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    // Read
    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    // Read by Id
    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    // Create
    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
    }

    // Delete
    public boolean delete(ProductoModel producto){
        try{
            productoRepository.delete(producto);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean existsByID(String id) {
        return(productoRepository.existsById(id));
    }
}
